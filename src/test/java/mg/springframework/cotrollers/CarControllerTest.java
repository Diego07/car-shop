package mg.springframework.cotrollers;

import mg.springframework.config.SecurityConfig;
import mg.springframework.model.Car;
import mg.springframework.service.CarService;
import mg.springframework.service.ImageService;
import mg.springframework.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Diego07 on 2018-02-21.
 */
public class CarControllerTest {

    @Mock
    CarService carService;

    @Mock
    ImageService imageService;

    @Mock
    UserService userService;

    @Mock
    Model model;

    CarController carController;

    MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        carController = new CarController(carService, imageService, userService);
        mockMvc = MockMvcBuilders.standaloneSetup(carController).build();
    }

    @Test
    public void testMockMVC() throws Exception {

        mockMvc.perform(get("/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("car-list"));

    }

    @Test
    public void getCarListPage() throws Exception {

        //given
        List<Car> carList = new ArrayList<>();
        carList.add(new Car());
        carList.add(new Car());

        when(carService.getCars()).thenReturn(carList);

        ArgumentCaptor<List<Car>> argumentCaptor = ArgumentCaptor.forClass(List.class);

        //when
        String viewName = carController.getCarListPage(model);

        //then
        assertEquals("car-list", viewName);
        verify(carService, times(1)).getCars();
        verify(model, times(1)).addAttribute(eq("cars"), argumentCaptor.capture());
        List<Car> setCarController = argumentCaptor.getValue();
        assertEquals(2, setCarController.size());

    }

    @Test
    public void newCar() throws Exception {

        mockMvc.perform(get("/car/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("car-form"))
                .andExpect(model().attributeExists("car"));
    }

    @Test
    public void updateCar() throws Exception {
        Car car = new Car();
        car.setId(2L);

        when(carService.findById(anyLong())).thenReturn(car);

        mockMvc.perform(get("/car/1/update"))
                .andExpect(status().isOk())
                .andExpect(view().name("car-form"))
                .andExpect(model().attributeExists("car"));
    }

    @Test
    public void deleteCarById() throws Exception {

        mockMvc.perform(get("/car/1/delete"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/list"));

        verify(carService, times(1)).deleteById(anyLong());
    }

    @Test
    public void renderImageFromDB() throws Exception {

        //given
        Car car = new Car();
        car.setId(1L);

        String s = "fake image";
        Byte[] bytesBoxed = new Byte[s.getBytes().length];

        int i = 0;

        for (byte primByte: s.getBytes()) {
            bytesBoxed[i++] = primByte;
        }

        car.setImage(bytesBoxed);

        when(carService.findById(anyLong())).thenReturn(car);

        //when
        MockHttpServletResponse response = mockMvc.perform(get("/car/1/carimage"))
                .andExpect(status().isOk())
                .andReturn().getResponse();

        byte[] responseBytes = response.getContentAsByteArray();

        assertEquals(s.getBytes().length, responseBytes.length);
    }
}