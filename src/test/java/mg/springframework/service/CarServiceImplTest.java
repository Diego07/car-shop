package mg.springframework.service;

import mg.springframework.model.Car;
import mg.springframework.repository.CarRepository;
import mg.springframework.repository.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

/**
 * Created by Diego07 on 2018-02-21.
 */
public class CarServiceImplTest {


    CarServiceImpl carService;

    @Mock
    CarRepository carRepository;

    @Mock
    UserRepository userRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        carService = new CarServiceImpl(carRepository, userRepository);
    }

    @Test
    public void getCars() throws Exception {

        Car car = new Car();
        List carData = new ArrayList();
        carData.add(car);

        when(carService.getCars()).thenReturn(carData);

        List<Car> carList = carService.getCars();

        assertEquals(carList.size(), 1);
        verify(carRepository, times(1)).findAll();
    }


    @Test
    public void findById() throws Exception {

        Car car = new Car();
        car.setId(1L);
        Optional<Car> carOptional = Optional.of(car);

        when(carRepository.findById(anyLong())).thenReturn(carOptional);

        Car carReturned = carService.findById(1L);

        assertNotNull("Null car returned", carReturned);
        verify(carRepository, times(1)).findById(anyLong());
        verify(carRepository, never()).findAll();
    }

}