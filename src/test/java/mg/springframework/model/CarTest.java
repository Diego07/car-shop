package mg.springframework.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Diego07 on 2018-02-21.
 */
public class CarTest {

    Car car;

    @Before
    public void setUp() {
        car = new Car();
    }

    @Test
    public void getId() throws Exception {
        Long idValue = 4L;

        car.setId(idValue);

        assertEquals(idValue, car.getId());
    }


}