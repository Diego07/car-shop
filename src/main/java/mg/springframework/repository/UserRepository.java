package mg.springframework.repository;

import mg.springframework.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Diego07 on 2018-01-10.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {


}
