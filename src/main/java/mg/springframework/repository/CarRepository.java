package mg.springframework.repository;

import mg.springframework.model.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Diego07 on 2017-11-13.
 */
@Repository
public interface CarRepository extends CrudRepository<Car, Long> {
}
