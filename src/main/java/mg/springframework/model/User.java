package mg.springframework.model;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diego07 on 2018-01-10.
 */
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Username cannot be empty")
    private String username;

    @NotEmpty(message = "Password cannot be empty")
    private String userpassword;

    private String role;

    @OneToMany (cascade = CascadeType.ALL, mappedBy = "user")
    private List<Car> boughtCarList = new ArrayList<>();


    public User() {
        this.role = "USER";
    }

    public List<Car> getBoughtCarList() {
        return boughtCarList;
    }

    public void setBoughtCarList(List<Car> boughtCarList) {
        this.boughtCarList = boughtCarList;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    private boolean enabled = true;

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpassword() {
        return userpassword;
    }

    public void setUserpassword(String userpassword) {
        this.userpassword = userpassword;
    }

    public User addCarToBoughtList(Car car) {
        car.setUser(this);
        this.boughtCarList.add(car);
        return this;
    }

    public void checkIfBought(Long id) {

        for (Car car: boughtCarList) {

            if (car.getId() == id) {
                car.setThisIsBoughtCar(true);
            }
        }
    }
}
