package mg.springframework.model;

/**
 * Created by Diego07 on 2017-11-17.
 */
public enum FuelType {

    PETROL, DIESEL, GAS, ELECTRIC
}
