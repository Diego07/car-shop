package mg.springframework.model;

import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * Created by Diego07 on 2017-11-13.
 */
@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty (message = "Mark cannot be empty")
    private String mark;

    @NotEmpty (message = "Model cannot be empty")
    private String model;

    @Enumerated(value = EnumType.STRING)
    private Condition condition;

    @NotNull (message = "Horse power cannot be empty")
    @Min (value = 2, message = "Horse power must have at least 2 digits")
    private int horsePower;

    @NotNull (message = "Year of production cannot be empty")
    @Min (value = 4, message = "Year of production must have 4 digits")
    private int yearOfProduction;

    @Enumerated(value = EnumType.STRING)
    private FuelType FuelType;

    @NotNull
    @Min (value = 1, message = "Price must have at least 1 digit")
    private int price;

    @Lob
    private String description;

    @Lob
    private Byte[] image;

    @ManyToOne
    private User user;

    private boolean thisIsBoughtCar;


    public boolean getThisIsBoughtCar() {
        return thisIsBoughtCar;
    }

    public void setThisIsBoughtCar(boolean isBought) {
        this.thisIsBoughtCar = isBought;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public int getYearOfProduction() {
        return yearOfProduction;
    }

    public void setYearOfProduction(int yearOfProduction) {
        this.yearOfProduction = yearOfProduction;
    }

    public FuelType getFuelType() {
        return FuelType;
    }

    public void setFuelType(FuelType fuelType) {
        FuelType = fuelType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }


}
