package mg.springframework.bootstrap;

import mg.springframework.model.*;
import mg.springframework.repository.CarRepository;
import mg.springframework.repository.UserRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Diego07 on 2017-11-13.
 */
@Component
public class CarBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private final CarRepository carRepository;
    private final UserRepository userRepository;

    public CarBootstrap(CarRepository carRepository, UserRepository userRepository) {
        this.carRepository = carRepository;
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        carRepository.saveAll(getCarList());
        userRepository.saveAll(getUserList());
    }

    private List<Car> getCarList() {

        List<Car> carList = new ArrayList<>(5);

        Car toyotaCar = new Car();

        toyotaCar.setCondition(Condition.NEW);
        toyotaCar.setDescription("Nowy samochod toyota w stanie idealnym, polecam!");
        toyotaCar.setFuelType(FuelType.PETROL);
        toyotaCar.setHorsePower(110);
        toyotaCar.setMark("Toyota");
        toyotaCar.setModel("Corolla");
        toyotaCar.setYearOfProduction(2014);
        toyotaCar.setPrice(9999);

        carList.add(toyotaCar);

        Car opelCar = new Car();

        opelCar.setCondition(Condition.USED);
        opelCar.setDescription("Uzywany opel, w stanie bardzo dobrym, bez rdzy, całkowicie sprawny mechanicznie");
        opelCar.setFuelType(FuelType.DIESEL);
        opelCar.setHorsePower(90);
        opelCar.setMark("Opel");
        opelCar.setModel("Astra");
        opelCar.setYearOfProduction(2001);
        opelCar.setPrice(14999);

        carList.add(opelCar);

        Car ferrariCar = new Car();

        ferrariCar.setCondition(Condition.NEW);
        ferrariCar.setDescription("Nowe ferrari");
        ferrariCar.setFuelType(FuelType.PETROL);
        ferrariCar.setHorsePower(490);
        ferrariCar.setMark("Ferrari");
        ferrariCar.setModel("Maranello");
        ferrariCar.setYearOfProduction(2014);
        ferrariCar.setPrice(999990);

        carList.add(ferrariCar);

        Car astonCar = new Car();

        astonCar.setCondition(Condition.NEW);
        astonCar.setDescription("Nowe aston martin od samego 007");
        astonCar.setFuelType(FuelType.PETROL);
        astonCar.setHorsePower(370);
        astonCar.setMark("Aston Martin");
        astonCar.setModel("DB9");
        astonCar.setYearOfProduction(2016);
        astonCar.setPrice(300000);

        carList.add(astonCar);

        Car daewooCar = new Car();

        daewooCar.setCondition(Condition.USED);
        daewooCar.setDescription("Samochod perelka");
        daewooCar.setFuelType(FuelType.DIESEL);
        daewooCar.setHorsePower(50);
        daewooCar.setMark("Daewoo");
        daewooCar.setModel("Tico");
        daewooCar.setYearOfProduction(1999);
        daewooCar.setPrice(500);

        carList.add(daewooCar);

        return carList;
    }

    private List<User> getUserList() {

        List<User> userList = new ArrayList<>(2);

        User user = new User();
        user.setUsername("user");
        user.setUserpassword("user");

        userList.add(user);

        User admin = new User();
        admin.setUsername("admin");
        admin.setUserpassword("admin");
        admin.setRole("ADMIN");

        userList.add(admin);

        return userList;
    }
 }
