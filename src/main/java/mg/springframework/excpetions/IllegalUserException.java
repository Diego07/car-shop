package mg.springframework.excpetions;

import mg.springframework.model.User;

/**
 * Created by Diego07 on 2018-03-25.
 */
public class IllegalUserException extends RuntimeException {

    public IllegalUserException(User user) {
        System.out.println("Username: " + user.getUsername() + " exists." );
    }

}
