package mg.springframework.cotrollers;

import mg.springframework.model.Car;
import mg.springframework.model.User;
import mg.springframework.service.CarService;
import mg.springframework.service.ImageService;
import mg.springframework.service.UserService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * Created by Diego07 on 2017-11-15.
 */
@Controller
public class CarController {

    private final CarService carService;
    private final ImageService imageService;
    private final UserService userService;

    public CarController(CarService carService, ImageService imageService, UserService userService) {
        this.carService = carService;
        this.imageService = imageService;
        this.userService = userService;
    }

    @GetMapping("/list")
    public String getCarListPage(Model model) {

        model.addAttribute("cars", carService.getCars());

            return "car-list";
    }

    @GetMapping("car/{id}/show")
    public String showCarById(@PathVariable String id, Model model) {

        model.addAttribute("car", carService.findById(new Long(id)));

        return "car-view";
    }

    @GetMapping("car/new")
    public String newCar(Model model) {

        model.addAttribute("car", new Car());

        return "car-form";
    }

    @PostMapping("newCar")
    public String addNewCar(@Valid @ModelAttribute("car") Car car,
                            BindingResult bindingResult, @RequestParam("imagefile") MultipartFile file) {

        if (bindingResult.hasErrors()) {

            bindingResult.getAllErrors().forEach(error -> System.out.println(error.getObjectName() + " " + error.getDefaultMessage()));
            return "car-form";

        } else {

            Car savedCar = carService.saveCar(car);
            imageService.saveImageFile(savedCar, file);

            return "redirect:/list";
        }
    }

    @GetMapping("car/{id}/carimage")
    public void renderImageFromDB(@PathVariable String id, HttpServletResponse response) throws IOException {

        Car car = carService.findById(Long.valueOf(id));

        if (car.getImage() != null) {
            byte[] byteArray = new byte[car.getImage().length];
            int i = 0;

            for (Byte wrappedByte : car.getImage()){
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream is = new ByteArrayInputStream(byteArray);
            IOUtils.copy(is, response.getOutputStream());
        }
    }

    @GetMapping("car/{id}/delete")
    public String deleteCarById(@PathVariable String id) {

        carService.deleteById(Long.valueOf(id));
        return "redirect:/list";
    }

    @GetMapping("car/{id}/update")
    public String updateCar(@PathVariable String id, Model model){

        model.addAttribute("car", carService.findById(Long.valueOf(id)));
        return "car-form";
    }

    @GetMapping("/search")
    public ModelAndView searchCar(@RequestParam("model") String query) {

        ModelAndView mav = new ModelAndView("car-list");
        mav.addObject("cars", carService.findByName(query));
        mav.addObject("searchQuery", query);
        return mav;
    }

    @GetMapping("car/{id}/buy")
    public String buyCar(@PathVariable String id){

        User user = userService.currentlyLoggedUser();
        Long currentUserId = user.getId();

        Car car = carService.findById(Long.valueOf(id));

        carService.saveBoughtCar(currentUserId, car);

        return "redirect:/list";
    }

    @GetMapping("/userdetails")
    public String showUserDetails(Model model) {

        User user = userService.currentlyLoggedUser();

        model.addAttribute("currentUser", user);
        model.addAttribute("boughtCars", user.getBoughtCarList());

        return "user-details";
    }
}
