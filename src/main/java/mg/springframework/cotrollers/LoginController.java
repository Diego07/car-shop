package mg.springframework.cotrollers;

import mg.springframework.excpetions.IllegalUserException;
import mg.springframework.model.User;
import mg.springframework.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



/**
 * Created by Diego07 on 2018-01-12.
 */

@Controller
public class LoginController {

    private final UserService userService;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/login")
    public String showLoginForm(Model model) {

        return "login-view";
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {

        model.addAttribute("logRegInfo", "Wrong username or password");
        model.addAttribute("alertClass", "alert-danger");

        return "login-view";
    }

    @RequestMapping("/register")
    public String showRegisterForm(Model model) {

        model.addAttribute("user", new User());
        return "register-view";
    }

    @PostMapping("register")
    public String addNewUser(@ModelAttribute("user") User user, Model model, RedirectAttributes
            redirectAttributes) {

            try {

                userService.saveUser(user);

                model.addAttribute("logRegInfo", "Register successful!");
                model.addAttribute("alertClass", "alert-success");

                return "login-view";

            } catch (IllegalUserException e) {

                redirectAttributes.addFlashAttribute("registerError", "User with username: " + user.getUsername()
                        + " already exists");
                redirectAttributes.addFlashAttribute("alertClass", "alert-danger");

                return "redirect:/register";
            }
    }
}
