package mg.springframework.cotrollers;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * Created by Diego07 on 2017-11-15.
 */
@Controller
public class IndexController {

    @RequestMapping({"", "/"})
    public String showIndex() {

    return "index";
    }

    @RequestMapping("/successful-logout")
    public String succesffulLogout(Model model) {

        model.addAttribute("logoutSuccess", "Logout successful!");
        model.addAttribute("alertClass", "alert-success");

        return "index";
    }
}
