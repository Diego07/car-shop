package mg.springframework.service;

import mg.springframework.model.Car;

import java.util.List;
import java.util.Set;

/**
 * Created by Diego07 on 2017-11-13.
 */
public interface CarService {

    List<Car> getCars();
    Car findById(Long id);
    Car saveCar(Car car);
    void deleteById(Long idToDelete);
    List<Car> findByName(String model);
    Car saveBoughtCar(Long id, Car car);
}
