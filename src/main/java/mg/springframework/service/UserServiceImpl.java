package mg.springframework.service;

import mg.springframework.excpetions.IllegalUserException;
import mg.springframework.model.User;
import mg.springframework.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * Created by Diego07 on 2018-01-10.
 */
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;


    @Autowired
    private EntityManager entityManager;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User saveUser(User user) throws IllegalUserException  {

        if (userExist(user.getUsername())) {

            throw new IllegalUserException(user);

        } else {

            userRepository.save(user);
        }
        return user;
    }

    @Override
    public User findUserByName(String name) {

        Query dbquery = entityManager.createQuery("FROM User u WHERE LOWER(u.username) LIKE :name");
        dbquery.setParameter("name", name.toLowerCase());
        User user = (User) dbquery.getResultList().stream().findFirst().orElse(null);

        return user;
    }

    private boolean userExist(String userName) {
        User user = findUserByName(userName);
        if (user != null) {
            return true;
        }
        return false;
    }

    @Override
    public User currentlyLoggedUser() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        return findUserByName(currentPrincipalName);
    }
}
