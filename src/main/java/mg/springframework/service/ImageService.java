package mg.springframework.service;

import mg.springframework.model.Car;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Diego07 on 2017-11-17.
 */
public interface ImageService {

    void saveImageFile(Car car, MultipartFile file);
}
