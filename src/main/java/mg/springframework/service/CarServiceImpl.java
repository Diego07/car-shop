package mg.springframework.service;

import mg.springframework.model.Car;
import mg.springframework.model.User;
import mg.springframework.repository.CarRepository;
import mg.springframework.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.*;

/**
 * Created by Diego07 on 2017-11-13.
 */
@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private EntityManager entityManager;

    private final CarRepository carRepository;
    private final UserRepository userRepository;

    public CarServiceImpl(CarRepository carRepository, UserRepository userRepository) {
        this.carRepository = carRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<Car> getCars() {

        List<Car> carList = new ArrayList<>();
        carRepository.findAll().iterator().forEachRemaining(carList::add);

        return carList;
    }

    @Override
    public Car findById(Long id) {

        Optional<Car> carOptional = carRepository.findById(id);
        return carOptional.get();
    }

    @Override
    public Car saveCar(Car car) {

        return carRepository.save(car);
    }

    @Override
    public void deleteById(Long idToDelete) {
        carRepository.deleteById(idToDelete);
    }

    @Override
    public List<Car> findByName(String model) {
        Query dbquery = entityManager.createQuery("FROM Car c WHERE LOWER(c.model) LIKE :model OR LOWER(c.mark) LIKE :model");
        dbquery.setParameter("model",'%' +  model.toLowerCase() + '%');
        List<Car> resultList = (List<Car>) dbquery.getResultList();
        return resultList;
    }

    @Override
    public Car saveBoughtCar(Long id, Car car) {

        Optional<User> userOptional = userRepository.findById(id);
        User user = userOptional.get();
        car.setUser(user);
        user.addCarToBoughtList(car);

        userRepository.save(user);

        Long carId = car.getId();
        user.checkIfBought(carId);

        return carRepository.save(car);
    }


}
