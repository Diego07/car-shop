package mg.springframework.service;

import mg.springframework.model.Car;
import mg.springframework.repository.CarRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;

/**
 * Created by Diego07 on 2017-11-17.
 */
@Service
public class ImageServiceImpl implements ImageService {

    private final CarRepository carRepository;

    public ImageServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Transactional
    @Override
    public void saveImageFile(Car car, MultipartFile file) {

        try {

            Byte[] byteObjects = new Byte[file.getBytes().length];

            int i = 0;

            for (byte b : file.getBytes()){
                byteObjects[i++] = b;
            }
            car.setImage(byteObjects);
            carRepository.save(car);
        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}

