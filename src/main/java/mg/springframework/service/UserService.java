package mg.springframework.service;

import mg.springframework.excpetions.IllegalUserException;
import mg.springframework.model.User;


/**
 * Created by Diego07 on 2018-01-10.
 */
public interface UserService {

    User saveUser(User user) throws IllegalUserException;
    User findUserByName(String name);
    User currentlyLoggedUser ();
}
